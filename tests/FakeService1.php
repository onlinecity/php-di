<?php namespace Onlinecity\Di\Tests;

class FakeService1
{
  protected $value;

  public function setValue($value)
  {
    $this->value = $value;
  }

  public function getValue()
  {
    return $this->value;
  }
}