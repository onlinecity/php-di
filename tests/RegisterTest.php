<?php namespace Onlinecity\Di\Tests;

use Onlinecity\Di\Container;
use Onlinecity\Di\Exception\ResolveException as ResolveException;

class RegisterTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var Container
   */
  protected $container;

  protected function setUp()
  {
    parent::setUp();
    $this->container = new Container();
  }

  protected function tearDown()
  {
    parent::tearDown();
    unset($this->container);
  }

  public function testResolveFailure()
  {
    $this->setExpectedException(get_class(new ResolveException()));
    $this->container->resolve('this_should_not_resolve');
  }

  public function testRegisterEquals()
  {
    $this->container->register(
      'fake_service',
      function () {
        return new FakeService1();
      }
    );
    $actual = $this->container->resolve('FakeService');
    $expected = new FakeService1();
    $this->assertEquals($expected, $actual);
  }

  public function testRegisterNotSame()
  {
    $this->container->register(
      'fake_service_1',
      function () {
        return new FakeService1();
      }
    );
    $actual = $this->container->resolve('FakeService1');
    $expected = new FakeService2();
    $this->assertNotEquals($expected, $actual);
  }

  public function testRegisterUsage()
  {
    $this->container->register(
      'fake_service_2',
      function () {
        return new FakeService2();
      }
    );
    $service = $this->container->resolve('FakeService2');
    $service->setValue('hello world');
    $this->assertEquals('hello world', $service->getValue());
  }
}
