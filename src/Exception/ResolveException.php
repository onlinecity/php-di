<?php namespace Onlinecity\Di\Exception;

/**
 * Dependency Injection Container ResolveException
 *
 * @package Onlinecity\Di
 * @author Jari Berg <jb@onlinecity.dk>
 */
class ResolveException extends \Exception {}
