<?php namespace Onlinecity\Di\Exception;

/**
 * Dependency Injection Container FrozenException
 *
 * @package Onlinecity\Di
 * @author Jari Berg <jb@onlinecity.dk>
 */
class FrozenException extends \Exception {}
