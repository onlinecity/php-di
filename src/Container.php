<?php namespace Onlinecity\Di;

use Onlinecity\Di\Exception\ResolveException;
use Onlinecity\Di\Exception\FrozenException;

/**
 * Dependency Injection Container
 *
 * @package Onlinecity\Di
 * @author Jari Berg <jb@onlinecity.dk>
 */
class Container
{
  /**
   * @var boolean
   */
  protected $frozen;

  /**
   * @var array
   */
  protected $values;

  /**
   * Constructor
   *
   * @param array $values [optional]
   */
  public function __construct(array $values = array())
  {
    $this->values = $values;
  }

  /**
   * Freeze container to reject any changes
   */
  public function freeze()
  {
    $this->frozen = true;
  }

  /**
   * Register service
   *
   * @param string $name
   * @param callable $callable
   */
  public function register($name, callable $callable)
  {
    $this->inject($name, $callable);
  }

  /**
   * Inject value into container with the specified name
   *
   * @param string $name
   * @param mixed $value
   *
   * @throws FrozenException
   */
  public function inject($name, $value)
  {
    if ($this->frozen === true) {
      throw new FrozenException('Container is frozen and should not be modified');
    }
    $this->values[$name] = $value;
  }

  /**
   * Check if value with the specified name exists in the container
   *
   * @param string $name
   *
   * @return boolean
   */
  public function exists($name)
  {
    return array_key_exists($name, $this->values);
  }

  /**
   * Resolve and return result
   *
   * @param string $name
   *
   * @return mixed
   * @throws ResolveException
   */
  public function resolve($name)
  {
    if (false === $this->exists($name)) {
      throw new ResolveException(sprintf('Could not resolve "%s" when no value exists', $name));
    }
    $value = $this->values[$name];
    if (is_callable($value)) {
      $value = call_user_func($value);
      $this->values[$name] = $value;
    }
    return $value;
  }
}
