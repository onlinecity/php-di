<?php namespace Onlinecity\Di;

/**
 * Dependency Injection Containerable Interface
 *
 * @package Onlinecity\Di
 * @author Jari Berg <jb@onlinecity.dk>
 */
interface ContainerableInterface
{
  /**
   * Set Container
   *
   * @param Container $container
   */
  public function setContainer(Container $container);
}